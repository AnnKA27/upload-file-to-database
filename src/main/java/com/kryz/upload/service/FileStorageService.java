package com.kryz.upload.service;

import com.kryz.upload.model.FileDB;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileStorageService {

    FileDB store(MultipartFile file) throws IOException;

    FileDB getFile(String id);

    List<FileDB> getAllFiles();
}
