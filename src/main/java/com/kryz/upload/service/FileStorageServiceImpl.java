package com.kryz.upload.service;

import com.kryz.upload.model.FileDB;
import com.kryz.upload.repository.FileDBRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class FileStorageServiceImpl implements FileStorageService {

    private final FileDBRepository repository;

    @Override
    public FileDB store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        return repository.save(new FileDB(fileName, file.getContentType(), file.getBytes()));
    }

    @Override
    public FileDB getFile(String id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public List<FileDB> getAllFiles() {
        return repository.findAll();
    }
}
